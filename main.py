from docxtpl import DocxTemplate
from docx2pdf import convert
from datetime import datetime
import os
from fastapi import FastAPI, File, UploadFile, Form
from fastapi.responses import HTMLResponse, FileResponse
app = FastAPI()


@app.get("/")
async def main1():
    content = """
<body>
<form action="/files/" enctype="multipart/form-data" method="post">
Шаблон<br>
<input name="file" type="file"><br>
Дата<br>
<input name="date" type="text"><br>
ФИО<br>
<input name="fio" type="text"><br>
ФИО сотрудника<br>
<input name="fio1" type="text"><br>
Должность<br>
<input name="work" type="text"><br>
<input type="submit">
</form>
</body>
    """
    return HTMLResponse(content=content)

@app.post("/files/")
async def create_upload_file(file: UploadFile = File(...), date: str = Form(...), fio: str = Form(...), fio1: str = Form(...), work: str = Form(...)):
    t1 = datetime.now().strftime("%d_%m_%y_%H_%M_%S_%f")
    filename = 'template\\'+file.filename
    with open(filename, 'wb') as out_file:
        out_file.write(file.file.read())
    print(date, fio, fio1, work)
    out_file = print_hi(filename, t1, date, fio, fio1, work)
    return FileResponse(out_file)

@app.get("/gen")
async def root():
    print_hi()
    return {"message": "Hello World"}


PATH = os.path.abspath(os.getcwd())


def print_hi(template_filename, t1, date, fio, fio1, work):
    in_file = 'tmp\\'+t1+".docx"
    out_file = PATH+'\\generated\\'+t1+".pdf"
    doc = DocxTemplate(template_filename)
    context = {
        'date': date,
        'ФИО_СДЛ_ИмП': fio,
        'Должность_сотрудника_ИмП_ДИ': work,
        'ФИО_сотрудника_ИмП_ДИ': fio1
    }
    doc.render(context)
    doc.save(in_file)
    convert(in_file, out_file)
    return out_file
    #os.remove(t+".docx")


if __name__ == '__main__':
    t = datetime.now().strftime("%d_%m_%y_%H_%M_%S_%f")
    print(PATH)
    start = datetime.now()
    print_hi("my_word_template.docx", t)
    end = datetime.now()
    print((end-start).microseconds / 1000)
